
#pragma once

#include "voxelGrid/VoxelGrid.h"

#include <utility>

template<typename Block_t, typename Index_t>
Index_t countActiveBits(const BitGrid3<Block_t, Index_t>& bGrid) {

	std::vector<bool> test;

	Index_t activeCount = 0;
	for (Index_t x = 0; x < bGrid.gridDims.x; ++x) {
		for (Index_t y = 0; y < bGrid.gridDims.y; ++y) {
			for (Index_t z = 0; z < bGrid.gridDims.z; ++z) {
				glm::tvec3<Index_t> position{ x,y,z };
				auto bit = bGrid[position];
				activeCount += (bit == true);
			}
		}
	}
	return activeCount;
}

template<typename Bitgrid_t, typename FP_t>
typename VoxelGrid<Bitgrid_t, FP_t>::index_type countActiveVoxels(const VoxelGrid<Bitgrid_t, FP_t>& vGrid) {

	using index_type = typename VoxelGrid<Bitgrid_t, FP_t>::index_type;

	std::vector<bool> test;

	index_type activeCount = 0;
	for (index_type x = 0; x < vGrid.gridDimensions().x; ++x) {
		for (index_type y = 0; y < vGrid.gridDimensions().y; ++y) {
			for (index_type z = 0; z < vGrid.gridDimensions().z; ++z) {
				glm::tvec3<index_type> position{ x,y,z };
				auto voxel = vGrid[position];
				activeCount += (voxel == ElementState::Active);
			}
		}
	}
	return activeCount;
}