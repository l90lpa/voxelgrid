
#include "catch.hpp"

#include <algorithm>
#include <array>

#include "voxelGrid/BitGrid.h"

#include "Test_Utilities.h"

using Block_type = uint32_t;
using Index_type = uint32_t;
using BitGrid = BitGrid3<Block_type, Index_type>;

SCENARIO("Constructing a bit grid with block type uint32_t and index type uint32_t", "[BG3_Construction]") {

	GIVEN("A bit grid constructed with 7^3 voxels and a block type of uint32_t") {

		const Index_type axisResolution = 7;
		const BitGrid bGrid{ {axisResolution, axisResolution, axisResolution} };

		WHEN("We check the number of voxels") {

			THEN("A count of 343 is returned") {
				REQUIRE(bGrid.numElements() == 343);
			}
		}

		AND_WHEN("We check the gridDims") {

			THEN("we find they are {7,7,7}") {

				REQUIRE(bGrid.gridDims == glm::tvec3<Index_type>{axisResolution, axisResolution, axisResolution});
			}
		}
	}

	GIVEN("A bit grid with 4^3 bits") {

		const Index_type axisResolution = 4;
		const BitGrid bGrid{ {axisResolution, axisResolution, axisResolution} };

		REQUIRE(bGrid.numElements() == 64);

		WHEN("we convert the position {3,3,3} to location") {

			const glm::tvec3<Index_type> position{ 3,3,3 };
			const auto location = bGrid.positionToLocation(position);

			THEN("the returned location is 63") {
				REQUIRE(location == 63);
			}
		}
	}

	GIVEN("A bit grid with 4^3 bits") {

		const Index_type axisResolution = 4;
		BitGrid bGrid{ {axisResolution, axisResolution, axisResolution} };

		REQUIRE(bGrid.numElements() == 64);

		WHEN("we set the bit at position {3,3,3}") {

			const glm::tvec3<Index_type> position{ 3,3,3 };
			bGrid[position] = true;

			THEN("on checking the bit at this location we find it is set to true") {
				REQUIRE(bGrid[position] == true);
			}
		}
	}

	GIVEN("A volume mesh with 4^3 voxels, a block type of uint32_t with the 8 corner voxels set") {

		uint32_t axisResolution = 4;
		BitGrid bGrid{ {axisResolution, axisResolution, axisResolution} };

		REQUIRE(bGrid.numElements() == 64);

		const std::array< glm::tvec3<uint32_t>, 8> cornerLocations{ {
			glm::tvec3<uint32_t>{0,0,0},
			glm::tvec3<uint32_t>{3,0,0},
			glm::tvec3<uint32_t>{0,3,0},
			glm::tvec3<uint32_t>{3,3,0},
			glm::tvec3<uint32_t>{0,0,3},
			glm::tvec3<uint32_t>{3,0,3},
			glm::tvec3<uint32_t>{0,3,3},
			glm::tvec3<uint32_t>{3,3,3}
		} };

		for (const auto& corner : cornerLocations) {
			bGrid[corner] = true;
		}

		WHEN("We count the number of active voxels") {

			Index_type activeCount = 0;

			for (Index_type x = 0; x < bGrid.gridDims.x; ++x) {
				for (Index_type y = 0; y < bGrid.gridDims.y; ++y) {
					for (Index_type z = 0; z < bGrid.gridDims.z; ++z) {
						glm::tvec3<Index_type> position{ x,y,z };

						auto bit = bGrid[position];
						if (bit) {
							++activeCount;
						}
					}
				}
			}

			THEN("A count of 8 is returned") {
				REQUIRE(activeCount == 8);
			}
		}

		AND_WHEN("We check the positions of the active voxels using random access") {

			std::vector<glm::tvec3<Index_type>> bitPositions;

			for (Index_type x = 0; x < bGrid.gridDims.x; ++x) {
				for (Index_type y = 0; y < bGrid.gridDims.y; ++y) {
					for (Index_type z = 0; z < bGrid.gridDims.z; ++z) {
						glm::tvec3<Index_type> position{ x,y,z };

						auto bit = bGrid[position];
						if (bit) {
							bitPositions.emplace_back(bit.position());
						}
					}
				}
			}

			REQUIRE(bitPositions.size() == 8);

			std::function<bool(glm::tvec3<Index_type>, glm::tvec3<Index_type>)> lexicographic_comparator = [](const auto& a, const auto& b) {
				if (a.z < b.z) {
					return true;
				}
				else if ((a.z == b.z) && (a.y < b.y)) {
					return true;
				}
				else if ((a.y == b.y) && (a.x < a.x)) {
					return true;
				}
				else {
					return false;
				}
			};

			std::sort(bitPositions.begin(), bitPositions.end(), lexicographic_comparator);

			THEN("The same corners are returned") {

				std::vector<glm::tvec3<Index_type>> difference;
				std::set_difference(cornerLocations.cbegin(), cornerLocations.cend(),
					bitPositions.cbegin(), bitPositions.cend(), std::back_inserter(difference), lexicographic_comparator);
				REQUIRE(difference.empty());
			}
		}

		AND_WHEN("We check the positions of the active voxels using an iterator") {

			std::vector<glm::tvec3<Index_type>> bitPositions;

			for (auto it = bGrid.begin(); it != bGrid.end(); ++it) {
				if (*it) {
					bitPositions.emplace_back((*it).position());
				}
			}

			REQUIRE(bitPositions.size() == 8);

			std::function<bool(glm::tvec3<Index_type>, glm::tvec3<Index_type>)> lexicographic_comparator = [](const auto& a, const auto& b) {
				if (a.z < b.z) {
					return true;
				}
				else if ((a.z == b.z) && (a.y < b.y)) {
					return true;
				}
				else if ((a.y == b.y) && (a.x < a.x)) {
					return true;
				}
				else {
					return false;
				}
			};

			std::sort(bitPositions.begin(), bitPositions.end(), lexicographic_comparator);

			THEN("The same corners are returned") {

				std::vector<glm::tvec3<Index_type>> difference;
				std::set_difference(cornerLocations.cbegin(), cornerLocations.cend(),
					bitPositions.cbegin(), bitPositions.cend(), std::back_inserter(difference), lexicographic_comparator);
				REQUIRE(difference.empty());
			}
		}
	}
}

SCENARIO("Setting and retrieving bits from a BitGrid3 using iterators", "[BG3_Iterator_Access]") {

	GIVEN("A bit grid with 4^3 voxels, a block type of uint32_t with the 8 corner voxels set") {

		const Index_type axisResolution = 4;
		BitGrid bGrid{ {axisResolution, axisResolution, axisResolution} };

		REQUIRE(bGrid.numElements() == 64);

		const std::array< glm::tvec3<Index_type>, 8> cornerBits{ {
			glm::tvec3<Index_type>{0,0,0},
			glm::tvec3<Index_type>{3,0,0},
			glm::tvec3<Index_type>{0,3,0},
			glm::tvec3<Index_type>{3,3,0},
			glm::tvec3<Index_type>{0,0,3},
			glm::tvec3<Index_type>{3,0,3},
			glm::tvec3<Index_type>{0,3,3},
			glm::tvec3<Index_type>{3,3,3}
		} };

		for (const auto& corner : cornerBits) {
			bGrid[corner] = true;
		}

		auto c_it = bGrid.cbegin();

		WHEN("We count the number of active voxels") {

			Index_type activeCount = countActiveBits(bGrid);

			THEN("A count of 8 is returned") {
				REQUIRE(activeCount == 8);
			}
		}

		AND_WHEN("We check the positions of the active voxels") {

			std::vector<glm::tvec3<Index_type>> bits;

			auto it = bGrid.begin();
			auto end = bGrid.end();
			for (; it != end; ++it) {
				if ((*it) == true) {
					bits.emplace_back(std::move((*it).position()));
				}
			}

			REQUIRE(bits.size() == 8);

			std::function<bool(glm::tvec3<Index_type>, glm::tvec3<Index_type>)> lexicographic_comparator = [](const auto& a, const auto& b) {
				if (a.z < b.z) {
					return true;
				}
				else if ((a.z == b.z) && (a.y < b.y)) {
					return true;
				}
				else if ((a.y == b.y) && (a.x < a.x)) {
					return true;
				}
				else {
					return false;
				}
			};

			std::sort(bits.begin(), bits.end(), lexicographic_comparator);

			THEN("The same corners are returned") {

				std::vector<glm::tvec3<Index_type>> difference;
				std::set_difference(cornerBits.cbegin(), cornerBits.cend(),
					bits.cbegin(), bits.cend(), std::back_inserter(difference), lexicographic_comparator);
				REQUIRE(difference.empty());
			}
		}
	}
}

SCENARIO("Testing BitGrid const iterator interface", "[Const_BG3_Iterator_Access]") {

	GIVEN("A bit grid with 4^3 voxels, a block type of uint32_t with the 8 corner voxels set") {

		const Index_type axisResolution = 4;
		BitGrid bGrid{ {axisResolution, axisResolution, axisResolution} };

		REQUIRE(bGrid.numElements() == 64);

		const std::array< glm::tvec3<Index_type>, 8> cornerBits{ {
			glm::tvec3<Index_type>{0,0,0},
			glm::tvec3<Index_type>{3,0,0},
			glm::tvec3<Index_type>{0,3,0},
			glm::tvec3<Index_type>{3,3,0},
			glm::tvec3<Index_type>{0,0,3},
			glm::tvec3<Index_type>{3,0,3},
			glm::tvec3<Index_type>{0,3,3},
			glm::tvec3<Index_type>{3,3,3}
		} };

		for (const auto& corner : cornerBits) {
			bGrid[corner] = true;
		}

		const auto constBGrid = bGrid;

		WHEN("We count the number of active voxels") {

			Index_type activeCount = countActiveBits(constBGrid);

			THEN("A count of 8 is returned") {
				REQUIRE(activeCount == 8);
			}
		}

		AND_WHEN("We check the positions of the active voxels") {

			std::vector<glm::tvec3<Index_type>> bits;

			auto it = constBGrid.begin();
			auto end = constBGrid.end();
			for (; it != end; ++it) {
				if ((*it) == true) {
					bits.emplace_back(std::move((*it).position()));
				}
			}

			REQUIRE(bits.size() == 8);

			std::function<bool(glm::tvec3<Index_type>, glm::tvec3<Index_type>)> lexicographic_comparator = [](const auto& a, const auto& b) {
				if (a.z < b.z) {
					return true;
				}
				else if ((a.z == b.z) && (a.y < b.y)) {
					return true;
				}
				else if ((a.y == b.y) && (a.x < a.x)) {
					return true;
				}
				else {
					return false;
				}
			};

			std::sort(bits.begin(), bits.end(), lexicographic_comparator);

			THEN("The same corners are returned") {

				std::vector<glm::tvec3<Index_type>> difference;
				std::set_difference(cornerBits.cbegin(), cornerBits.cend(),
					bits.cbegin(), bits.cend(), std::back_inserter(difference), lexicographic_comparator);
				REQUIRE(difference.empty());
			}
		}
	}
}