
#include "catch.hpp"

#include <algorithm>
#include <array>
#include <glm/gtc/epsilon.hpp>
#include <glm/glm.hpp>

#include "voxelGrid/VoxelGrid.h"

#include "Test_Utilities.h"

using Index_type = uint32_t;
using Block_type = uint32_t;
using Bitgrid_type = BitGrid3<Block_type, Index_type>;
using FP_type = float;

using VGrid = VoxelGrid<Bitgrid_type, FP_type>;

SCENARIO("Constructing a VoxelGrid with block type uint32_t and index type uint32_t", "[VM2_Construction]") {

	GIVEN("A voxel grdi constructed with 7^3 voxels and a block type of uint32_t") {

		const Index_type axisResolution = 7;
		const FP_type unitLength = 1.0;
		const glm::tvec3<FP_type> aabbMin{0.0, 0.0, 0.0};
		const glm::tvec3<FP_type> aabbMax{7.0, 7.0, 7.0};

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{aabbMin, aabbMax};

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		WHEN("We check the number of voxels") {

			THEN("A count of 343 is returned") {
				REQUIRE(vGrid.numVoxels() == 343);
			}
		}

		AND_WHEN("We check the gridDims") {

			THEN("we find they are {7,7,7}") {

				REQUIRE(vGrid.gridDimensions() == gridDims);
			}
		}
	}

	GIVEN("A voxel grid with 4^3 voxels, a block type of uint32_t with the 8 corner voxels set") {

		const Index_type axisResolution = 4;
		const FP_type unitLength = 1.0;
		const glm::tvec3<FP_type> aabbMin{ 0.0, 0.0, 0.0 };
		const glm::tvec3<FP_type> aabbMax{ 4.0, 4.0, 4.0 };

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		REQUIRE(vGrid.numVoxels() == 64);

		const std::array< glm::tvec3<Index_type>, 8> cornerVoxels{ {
			glm::tvec3<Index_type>{0,0,0},
			glm::tvec3<Index_type>{3,0,0},
			glm::tvec3<Index_type>{0,3,0},
			glm::tvec3<Index_type>{3,3,0},
			glm::tvec3<Index_type>{0,0,3},
			glm::tvec3<Index_type>{3,0,3},
			glm::tvec3<Index_type>{0,3,3},
			glm::tvec3<Index_type>{3,3,3}
		} };

		for (const auto& corner : cornerVoxels) {
			vGrid[corner] = ElementState::Active;
		}

		WHEN("We count the number of active voxels") {

			Index_type activeCount = 0;

			for (Index_type x = 0; x < vGrid.gridDimensions().x; ++x) {
				for (Index_type y = 0; y < vGrid.gridDimensions().y; ++y) {
					for (Index_type z = 0; z < vGrid.gridDimensions().z; ++z) {
						glm::tvec3<Index_type> position{ x,y,z };

						auto voxel = vGrid[position];
						if (voxel == ElementState::Active) {
							++activeCount;
						}
					}
				}
			}

			THEN("A count of 8 is returned") {
				REQUIRE(activeCount == 8);
			}
		}

		AND_WHEN("We check the positions of the active voxels") {

			std::vector<glm::tvec3<Index_type>> voxels;

			for (Index_type x = 0; x < vGrid.gridDimensions().x; ++x) {
				for (Index_type y = 0; y < vGrid.gridDimensions().y; ++y) {
					for (Index_type z = 0; z < vGrid.gridDimensions().z; ++z) {
						glm::tvec3<Index_type> position{ x,y,z };

						auto voxel = vGrid[position];
						if (voxel == ElementState::Active) {
							voxels.emplace_back(std::move(position));
						}
					}
				}
			}

			REQUIRE(voxels.size() == 8);

			std::function<bool(glm::tvec3<Index_type>, glm::tvec3<Index_type>)> lexicographic_comparator = [](const auto& a, const auto& b) {
				if (a.z < b.z) {
					return true;
				}
				else if ((a.z == b.z) && (a.y < b.y)) {
					return true;
				}
				else if ((a.y == b.y) && (a.x < a.x)) {
					return true;
				}
				else {
					return false;
				}
			};

			std::sort(voxels.begin(), voxels.end(), lexicographic_comparator);

			THEN("The same corners are returned") {

				std::vector<glm::tvec3<Index_type>> difference;
				std::set_difference(cornerVoxels.cbegin(), cornerVoxels.cend(),
					voxels.cbegin(), voxels.cend(), std::back_inserter(difference), lexicographic_comparator);
				REQUIRE(difference.empty());
			}
		}
	}
}

SCENARIO("Setting and retrieving voxels from a VoxelGrid using random access via position", "[VM2_Random_Access_Position]") {

	GIVEN("A voxel grid with 4^3 voxels, a block type of uint32_t with the 8 corner voxels set") {

		const Index_type axisResolution = 4;
		const FP_type unitLength = 1.0;
		const glm::tvec3<FP_type> aabbMin{ 0.0, 0.0, 0.0 };
		const glm::tvec3<FP_type> aabbMax{ 4.0, 4.0, 4.0 };

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		REQUIRE(vGrid.numVoxels() == 64);

		const std::array< glm::tvec3<Index_type>, 8> cornerVoxels{ {
			glm::tvec3<Index_type>{0,0,0},
			glm::tvec3<Index_type>{3,0,0},
			glm::tvec3<Index_type>{0,3,0},
			glm::tvec3<Index_type>{3,3,0},
			glm::tvec3<Index_type>{0,0,3},
			glm::tvec3<Index_type>{3,0,3},
			glm::tvec3<Index_type>{0,3,3},
			glm::tvec3<Index_type>{3,3,3}
		} };

		for (const auto& corner : cornerVoxels) {
			vGrid[corner] = ElementState::Active;
		}

		WHEN("We count the number of active voxels") {

			Index_type activeCount = countActiveVoxels(vGrid);

			THEN("A count of 8 is returned") {
				REQUIRE(activeCount == 8);
			}
		}

		AND_WHEN("We check the positions of the active voxels") {

			std::vector<glm::tvec3<Index_type>> voxels;

			for (Index_type x = 0; x < vGrid.gridDimensions().x; ++x) {
				for (Index_type y = 0; y < vGrid.gridDimensions().y; ++y) {
					for (Index_type z = 0; z < vGrid.gridDimensions().z; ++z) {
						glm::tvec3<Index_type> position{ x,y,z };

						auto voxel = vGrid[position];
						if (voxel == ElementState::Active) {
							voxels.emplace_back(std::move(position));
						}
					}
				}
			}

			REQUIRE(voxels.size() == 8);

			std::function<bool(glm::tvec3<Index_type>, glm::tvec3<Index_type>)> lexicographic_comparator = [](const auto& a, const auto& b) {
				if (a.z < b.z) {
					return true;
				}
				else if ((a.z == b.z) && (a.y < b.y)) {
					return true;
				}
				else if ((a.y == b.y) && (a.x < a.x)) {
					return true;
				}
				else {
					return false;
				}
			};

			std::sort(voxels.begin(), voxels.end(), lexicographic_comparator);

			THEN("The same corners are returned") {

				std::vector<glm::tvec3<Index_type>> difference;
				std::set_difference(cornerVoxels.cbegin(), cornerVoxels.cend(),
					voxels.cbegin(), voxels.cend(), std::back_inserter(difference), lexicographic_comparator);
				REQUIRE(difference.empty());
			}
		}
	}
}

SCENARIO("Setting and retrieving voxels from a VoxelGrid using random access via index", "[VM2_Random_Access_Index]") {

	GIVEN("A voxel grid with 4^3 voxels, a block type of uint32_t with the 8 corner voxels set") {

		const Index_type axisResolution = 4;
		const FP_type unitLength = 1.0;
		const glm::tvec3<FP_type> aabbMin{ 0.0, 0.0, 0.0 };
		const glm::tvec3<FP_type> aabbMax{ 4.0, 4.0, 4.0 };

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		REQUIRE(vGrid.numVoxels() == 64);

		const std::array< glm::tvec3<Index_type>, 8> cornerVoxels{ {
			glm::tvec3<Index_type>{0,0,0},
			glm::tvec3<Index_type>{3,0,0},
			glm::tvec3<Index_type>{0,3,0},
			glm::tvec3<Index_type>{3,3,0},
			glm::tvec3<Index_type>{0,0,3},
			glm::tvec3<Index_type>{3,0,3},
			glm::tvec3<Index_type>{0,3,3},
			glm::tvec3<Index_type>{3,3,3}
		} };

		for (const auto& corner : cornerVoxels) {
			auto index = vGrid.positionToLocation(corner);
			vGrid[index] = ElementState::Active;
		}

		WHEN("We count the number of active voxels") {

			Index_type activeCount = countActiveVoxels(vGrid);

			THEN("A count of 8 is returned") {
				REQUIRE(activeCount == 8);
			}
		}

		AND_WHEN("We check the positions of the active voxels") {

			std::vector<glm::tvec3<Index_type>> voxels;

			for (size_t i = 0; i < vGrid.numVoxels(); ++i) {
				if (vGrid[i] == ElementState::Active) {
					voxels.emplace_back(vGrid[i].gridPosition());
				}
			}

			REQUIRE(voxels.size() == 8);

			std::function<bool(glm::tvec3<Index_type>, glm::tvec3<Index_type>)> lexicographic_comparator = [](const auto& a, const auto& b) {
				if (a.z < b.z) {
					return true;
				}
				else if ((a.z == b.z) && (a.y < b.y)) {
					return true;
				}
				else if ((a.y == b.y) && (a.x < a.x)) {
					return true;
				}
				else {
					return false;
				}
			};

			std::sort(voxels.begin(), voxels.end(), lexicographic_comparator);

			THEN("The same corners are returned") {

				std::vector<glm::tvec3<Index_type>> difference;
				std::set_difference(cornerVoxels.cbegin(), cornerVoxels.cend(),
					voxels.cbegin(), voxels.cend(), std::back_inserter(difference), lexicographic_comparator);
				REQUIRE(difference.empty());
			}
		}
	}
}

SCENARIO("Setting and retrieving voxels from a VoxelGrid using iterators", "[VM2_Iterator_Access]") {

	GIVEN("A voxel grid with 4^3 voxels, a block type of uint32_t with the 8 corner voxels set") {

		const Index_type axisResolution = 4;
		const FP_type unitLength = 1.0;
		const glm::tvec3<FP_type> aabbMin{ 0.0, 0.0, 0.0 };
		const glm::tvec3<FP_type> aabbMax{ 4.0, 4.0, 4.0 };

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		REQUIRE(vGrid.numVoxels() == 64);

		const std::array< glm::tvec3<Index_type>, 8> cornerVoxels{ {
			glm::tvec3<Index_type>{0,0,0},
			glm::tvec3<Index_type>{3,0,0},
			glm::tvec3<Index_type>{0,3,0},
			glm::tvec3<Index_type>{3,3,0},
			glm::tvec3<Index_type>{0,0,3},
			glm::tvec3<Index_type>{3,0,3},
			glm::tvec3<Index_type>{0,3,3},
			glm::tvec3<Index_type>{3,3,3}
		} };

		for (const auto& corner : cornerVoxels) {
			vGrid[corner] = ElementState::Active;
		}

		WHEN("We count the number of active voxels") {

			Index_type activeCount = countActiveVoxels(vGrid);

			THEN("A count of 8 is returned") {
				REQUIRE(activeCount == 8);
			}
		}

		AND_WHEN("We check the positions of the active voxels") {

			std::vector<glm::tvec3<Index_type>> voxels;

			auto it = vGrid.begin();
			auto end = vGrid.end();
			for (; it != end; ++it) {
				if ((*it) == ElementState::Active) {
					voxels.emplace_back(std::move((*it).gridPosition()));
				}
			}

			REQUIRE(voxels.size() == 8);

			std::function<bool(glm::tvec3<Index_type>, glm::tvec3<Index_type>)> lexicographic_comparator = [](const auto& a, const auto& b) {
				if (a.z < b.z) {
					return true;
				}
				else if ((a.z == b.z) && (a.y < b.y)) {
					return true;
				}
				else if ((a.y == b.y) && (a.x < a.x)) {
					return true;
				}
				else {
					return false;
				}
			};

			std::sort(voxels.begin(), voxels.end(), lexicographic_comparator);

			THEN("The same corners are returned") {

				std::vector<glm::tvec3<Index_type>> difference;
				std::set_difference(cornerVoxels.cbegin(), cornerVoxels.cend(),
					voxels.cbegin(), voxels.cend(), std::back_inserter(difference), lexicographic_comparator);
				REQUIRE(difference.empty());
			}
		}
	}
}

SCENARIO("Testing the const random access interface via position", "[Const_VM2_Random_Access_Position]") {

	GIVEN("A const voxel grid with 4^3 voxels, a block type of uint32_t with the 8 corner voxels set") {

		const Index_type axisResolution = 4;
		const FP_type unitLength = 1.0;
		const glm::tvec3<FP_type> aabbMin{ 0.0, 0.0, 0.0 };
		const glm::tvec3<FP_type> aabbMax{ 4.0, 4.0, 4.0 };

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		REQUIRE(vGrid.numVoxels() == 64);

		const std::array< glm::tvec3<Index_type>, 8> cornerVoxels{ {
			glm::tvec3<Index_type>{0,0,0},
			glm::tvec3<Index_type>{3,0,0},
			glm::tvec3<Index_type>{0,3,0},
			glm::tvec3<Index_type>{3,3,0},
			glm::tvec3<Index_type>{0,0,3},
			glm::tvec3<Index_type>{3,0,3},
			glm::tvec3<Index_type>{0,3,3},
			glm::tvec3<Index_type>{3,3,3}
		} };

		for (const auto& corner : cornerVoxels) {
			vGrid[corner] = ElementState::Active;
		}

		const VGrid constVGrid = vGrid;

		WHEN("We count the number of active voxels") {

			Index_type activeCount = countActiveVoxels(constVGrid);

			THEN("A count of 8 is returned") {
				REQUIRE(activeCount == 8);
			}
		}

		AND_WHEN("We check the positions of the active voxels") {

			std::vector<glm::tvec3<Index_type>> voxels;

			for (Index_type x = 0; x < constVGrid.gridDimensions().x; ++x) {
				for (Index_type y = 0; y < constVGrid.gridDimensions().y; ++y) {
					for (Index_type z = 0; z < constVGrid.gridDimensions().z; ++z) {
						glm::tvec3<Index_type> position{ x,y,z };

						auto voxel = constVGrid[position];
						if (voxel == ElementState::Active) {
							voxels.emplace_back(std::move(position));
						}
					}
				}
			}

			REQUIRE(voxels.size() == 8);

			std::function<bool(glm::tvec3<Index_type>, glm::tvec3<Index_type>)> lexicographic_comparator = [](const auto& a, const auto& b) {
				if (a.z < b.z) {
					return true;
				}
				else if ((a.z == b.z) && (a.y < b.y)) {
					return true;
				}
				else if ((a.y == b.y) && (a.x < a.x)) {
					return true;
				}
				else {
					return false;
				}
			};

			std::sort(voxels.begin(), voxels.end(), lexicographic_comparator);

			THEN("The same corners are returned") {

				std::vector<glm::tvec3<Index_type>> difference;
				std::set_difference(cornerVoxels.cbegin(), cornerVoxels.cend(),
					voxels.cbegin(), voxels.cend(), std::back_inserter(difference), lexicographic_comparator);
				REQUIRE(difference.empty());
			}
		}
	}
}

SCENARIO("Testing the const random access interface via index", "[Const_VM2_Random_Access_Index]") {

	GIVEN("A voxel grid with 4^3 voxels, a block type of uint32_t with the 8 corner voxels set") {

		const Index_type axisResolution = 4;
		const FP_type unitLength = 1.0;
		const glm::tvec3<FP_type> aabbMin{ 0.0, 0.0, 0.0 };
		const glm::tvec3<FP_type> aabbMax{ 4.0, 4.0, 4.0 };

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		REQUIRE(vGrid.numVoxels() == 64);

		const std::array< glm::tvec3<Index_type>, 8> cornerVoxels{ {
			glm::tvec3<Index_type>{0,0,0},
			glm::tvec3<Index_type>{3,0,0},
			glm::tvec3<Index_type>{0,3,0},
			glm::tvec3<Index_type>{3,3,0},
			glm::tvec3<Index_type>{0,0,3},
			glm::tvec3<Index_type>{3,0,3},
			glm::tvec3<Index_type>{0,3,3},
			glm::tvec3<Index_type>{3,3,3}
		} };

		for (const auto& corner : cornerVoxels) {
			auto index = vGrid.positionToLocation(corner);
			vGrid[index] = ElementState::Active;
		}

		const VGrid constVGrid = vGrid;

		WHEN("We count the number of active voxels") {

			Index_type activeCount = countActiveVoxels(constVGrid);

			THEN("A count of 8 is returned") {
				REQUIRE(activeCount == 8);
			}
		}

		AND_WHEN("We check the positions of the active voxels") {

			std::vector<glm::tvec3<Index_type>> voxels;

			for (size_t i = 0; i < constVGrid.numVoxels(); ++i) {
				if (constVGrid[i] == ElementState::Active) {
					voxels.emplace_back(constVGrid[i].gridPosition());
				}
			}

			REQUIRE(voxels.size() == 8);

			std::function<bool(glm::tvec3<Index_type>, glm::tvec3<Index_type>)> lexicographic_comparator = [](const auto& a, const auto& b) {
				if (a.z < b.z) {
					return true;
				}
				else if ((a.z == b.z) && (a.y < b.y)) {
					return true;
				}
				else if ((a.y == b.y) && (a.x < a.x)) {
					return true;
				}
				else {
					return false;
				}
			};

			std::sort(voxels.begin(), voxels.end(), lexicographic_comparator);

			THEN("The same corners are returned") {

				std::vector<glm::tvec3<Index_type>> difference;
				std::set_difference(cornerVoxels.cbegin(), cornerVoxels.cend(),
					voxels.cbegin(), voxels.cend(), std::back_inserter(difference), lexicographic_comparator);
				REQUIRE(difference.empty());
			}
		}
	}
}

SCENARIO("Testing the const iterators", "[Const_VM2_Iterator_Access]") {

	GIVEN("A voxel grid with 4^3 voxels, a block type of uint32_t with the 8 corner voxels set") {

		const Index_type axisResolution = 4;
		const FP_type unitLength = 1.0;
		const glm::tvec3<FP_type> aabbMin{ 0.0, 0.0, 0.0 };
		const glm::tvec3<FP_type> aabbMax{ 4.0, 4.0, 4.0 };

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		REQUIRE(vGrid.numVoxels() == 64);

		const std::array< glm::tvec3<Index_type>, 8> cornerVoxels{ {
			glm::tvec3<Index_type>{0,0,0},
			glm::tvec3<Index_type>{3,0,0},
			glm::tvec3<Index_type>{0,3,0},
			glm::tvec3<Index_type>{3,3,0},
			glm::tvec3<Index_type>{0,0,3},
			glm::tvec3<Index_type>{3,0,3},
			glm::tvec3<Index_type>{0,3,3},
			glm::tvec3<Index_type>{3,3,3}
		} };

		for (const auto& corner : cornerVoxels) {
			vGrid[corner] = ElementState::Active;
		}

		const VGrid constVGrid = vGrid;

		WHEN("We count the number of active voxels") {

			Index_type activeCount = countActiveVoxels(constVGrid);

			THEN("A count of 8 is returned") {
				REQUIRE(activeCount == 8);
			}
		}

		AND_WHEN("We check the positions of the active voxels") {

			std::vector<glm::tvec3<Index_type>> voxels;

			auto it = constVGrid.begin();
			auto end = constVGrid.end();
			for (; it != end; ++it) {
				if ((*it) == ElementState::Active) {
					voxels.emplace_back(std::move((*it).gridPosition()));
				}
			}

			REQUIRE(voxels.size() == 8);

			std::function<bool(glm::tvec3<Index_type>, glm::tvec3<Index_type>)> lexicographic_comparator = [](const auto& a, const auto& b) {
				if (a.z < b.z) {
					return true;
				}
				else if ((a.z == b.z) && (a.y < b.y)) {
					return true;
				}
				else if ((a.y == b.y) && (a.x < a.x)) {
					return true;
				}
				else {
					return false;
				}
			};

			std::sort(voxels.begin(), voxels.end(), lexicographic_comparator);

			THEN("The same corners are returned") {

				std::vector<glm::tvec3<Index_type>> difference;
				std::set_difference(cornerVoxels.cbegin(), cornerVoxels.cend(),
					voxels.cbegin(), voxels.cend(), std::back_inserter(difference), lexicographic_comparator);
				REQUIRE(difference.empty());
			}
		}
	}
}

SCENARIO("Testing the calculation of world position", "[VM2_World_Position]") {

	constexpr FP_type epsilon = 0.1;

	GIVEN("a voxel grid") {

	const Index_type axisResolution = 4;
	const FP_type unitLength = 2.0;
	const glm::tvec3<FP_type> aabbMin{ 0.5, 0.5, 0.5 };
	const glm::tvec3<FP_type> aabbMax{ 8.5, 8.5, 8.5 };

	const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
	const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
	const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

	VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		WHEN("we get the world position at voxel {0,0,0}") {
			THEN("it is close to {0.5, 0.5, 0.5}") {
				REQUIRE(glm::all(glm::epsilonEqual(vGrid[glm::tvec3<Index_type>{0, 0, 0}].worldPosition(), glm::tvec3<FP_type>{0.5, 0.5, 0.5}, epsilon)));
			}
		}
		AND_WHEN("we get the world position at voxel {1,1,1}") {
			THEN("it is close to {2.5, 2.5, 2.5}") {
				REQUIRE(glm::all(glm::epsilonEqual(vGrid[glm::tvec3<Index_type>{1, 1, 1}].worldPosition(), glm::tvec3<FP_type>{2.5, 2.5, 2.5}, epsilon)));
			}
		}
		AND_WHEN("we get the world position at voxel {2,2,2}") {
			THEN("it is close to {4.5, 4.5, 4.5}") {
				REQUIRE(glm::all(glm::epsilonEqual(vGrid[glm::tvec3<Index_type>{2, 2, 2}].worldPosition(), glm::tvec3<FP_type>{4.5, 4.5, 4.5}, epsilon)));
			}
		}
		AND_WHEN("we get the world position at voxel {3,3,3}") {
			THEN("it is close to {6.5, 6.5, 6.5}") {
				REQUIRE(glm::all(glm::epsilonEqual(vGrid[glm::tvec3<Index_type>{3, 3, 3}].worldPosition(), glm::tvec3<FP_type>{6.5, 6.5, 6.5}, epsilon)));
			}
		}
	}
}

SCENARIO("Testing the calculation of world position via the const interface", "[Const_VM2_World_Position]") {

	constexpr FP_type epsilon = 0.1;

	GIVEN("a voxel grid") {

		const Index_type axisResolution = 4;
		const FP_type unitLength = 2.0;
		const glm::tvec3<FP_type> aabbMin{ 0.5, 0.5, 0.5 };
		const glm::tvec3<FP_type> aabbMax{ 8.5, 8.5, 8.5 };

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		const auto constVGrid = vGrid;

		WHEN("we get the world position at voxel {0,0,0}") {
			THEN("it is close to {0.5, 0.5, 0.5}") {
				REQUIRE(glm::all(glm::epsilonEqual(constVGrid[glm::tvec3<Index_type>{0, 0, 0}].worldPosition(), glm::tvec3<FP_type>{0.5, 0.5, 0.5}, epsilon)));
			}
		}
		AND_WHEN("we get the world position at voxel {1,1,1}") {
			THEN("it is close to {2.5, 2.5, 2.5}") {
				REQUIRE(glm::all(glm::epsilonEqual(constVGrid[glm::tvec3<Index_type>{1, 1, 1}].worldPosition(), glm::tvec3<FP_type>{2.5, 2.5, 2.5}, epsilon)));
			}
		}
		AND_WHEN("we get the world position at voxel {2,2,2}") {
			THEN("it is close to {4.5, 4.5, 4.5}") {
				REQUIRE(glm::all(glm::epsilonEqual(constVGrid[glm::tvec3<Index_type>{2, 2, 2}].worldPosition(), glm::tvec3<FP_type>{4.5, 4.5, 4.5}, epsilon)));
			}
		}
		AND_WHEN("we get the world position at voxel {3,3,3}") {
			THEN("it is close to {6.5, 6.5, 6.5}") {
				REQUIRE(glm::all(glm::epsilonEqual(constVGrid[glm::tvec3<Index_type>{3, 3, 3}].worldPosition(), glm::tvec3<FP_type>{6.5, 6.5, 6.5}, epsilon)));
			}
		}
	}
}