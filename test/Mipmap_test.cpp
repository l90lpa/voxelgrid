
#include "catch.hpp"

#include <algorithm>
#include <array>

#include "voxelGrid/Mipmap.h"
#include "Test_Utilities.h"


SCENARIO("Mipmapping a BitGrid3 with block type uint32_t and index type uint32_t", "[BG3_Mipmap]") {

	using Block_type = uint32_t;
	using Index_type = uint32_t;
	using BGrid = BitGrid3<Block_type, Index_type>;

	GIVEN("An empty voxel grid") {

		Index_type axisResolution = 0;
		BGrid bGrid{ {axisResolution, axisResolution, axisResolution} };

		WHEN("We mipmap with no requested levels") {

			auto mipmapLevels = mipmap(bGrid);

			THEN("No mipmap levels are returned") {
				REQUIRE(mipmapLevels.empty());
			}
		}

		AND_WHEN("We mipmap with 3 requested levels") {

			auto mipmapLevels = mipmap(bGrid, 3);

			THEN("No mipmap levels are returned") {
				REQUIRE(mipmapLevels.empty());
			}
		}
	}

	GIVEN("A voxel grid with 4^3 voxels, with the 8 corner voxels set") {

		Index_type axisResolution = 4;
		BGrid bGrid{ {axisResolution, axisResolution, axisResolution} };

		REQUIRE(bGrid.numElements() == 64);

		const std::array< glm::tvec3<Index_type>, 8> cornerLocations{ {
			glm::tvec3<Index_type>{0,0,0},
			glm::tvec3<Index_type>{3,0,0},
			glm::tvec3<Index_type>{0,3,0},
			glm::tvec3<Index_type>{3,3,0},
			glm::tvec3<Index_type>{0,0,3},
			glm::tvec3<Index_type>{3,0,3},
			glm::tvec3<Index_type>{0,3,3},
			glm::tvec3<Index_type>{3,3,3}
		} };

		for (const auto& corner : cornerLocations) {
			bGrid[corner] = true;
		}

		WHEN("We mipmap the voxel grid") {

			auto mipmapLevels = mipmap(bGrid);

			THEN("2 mipmap levels are returned. The first level with gridDims {2,2,2} and all voxels active. The second level with gridDims {1,1,1} and its element active") {
				REQUIRE(mipmapLevels.size() == 2);
				REQUIRE(mipmapLevels[0].gridDims == glm::tvec3<Index_type>{2, 2, 2});
				REQUIRE(mipmapLevels[1].gridDims == glm::tvec3<Index_type>{1, 1, 1});


				Index_type activeCount_0 = countActiveBits(mipmapLevels[0]);

				REQUIRE(activeCount_0 == 8);

				Index_type activeCount_1 = countActiveBits(mipmapLevels[1]);

				REQUIRE(activeCount_1 == 1);

			}
		}

	}

	GIVEN("A voxel grid with 7^3 voxels, with the 8 corner voxels set") {

		Index_type axisResolution = 7;
		BGrid bGrid{ {axisResolution, axisResolution, axisResolution} };;

		const std::array< glm::tvec3<Index_type>, 8> cornerLocations{ {
			glm::tvec3<Index_type>{0,0,0},
			glm::tvec3<Index_type>{6,0,0},
			glm::tvec3<Index_type>{0,6,0},
			glm::tvec3<Index_type>{6,6,0},
			glm::tvec3<Index_type>{0,0,6},
			glm::tvec3<Index_type>{6,0,6},
			glm::tvec3<Index_type>{0,6,6},
			glm::tvec3<Index_type>{6,6,6}
		} };

		for (const auto& corner : cornerLocations) {
			bGrid[corner] = true;
		}

		WHEN("We mipmap the voxel grid") {

			auto mipmapLevels = mipmap(bGrid);

			THEN("2 mipmap levels are returned. The first level with gridDims {2,2,2} and all voxels active. The second level with gridDims {1,1,1} and its element active") {
				REQUIRE(mipmapLevels.size() == 3);
				REQUIRE(mipmapLevels[0].gridDims == glm::tvec3<Index_type>{4, 4, 4});
				REQUIRE(mipmapLevels[1].gridDims == glm::tvec3<Index_type>{2, 2, 2});
				REQUIRE(mipmapLevels[2].gridDims == glm::tvec3<Index_type>{1, 1, 1});


				Index_type activeCount_0 = countActiveBits(mipmapLevels[0]);
				Index_type activeCount_1 = countActiveBits(mipmapLevels[1]);
				Index_type activeCount_2 = countActiveBits(mipmapLevels[2]);

				REQUIRE(activeCount_0 == 8);
				REQUIRE(activeCount_1 == 8);
				REQUIRE(activeCount_2 == 1);

			}
		}

	}


}


SCENARIO("Mipmapping a voxel grid with block type uint32_t and index type uint32_t", "[VM2_Mipmap]") {


	using BGrid = BitGrid3<uint32_t, uint32_t>;
	using FP_type = float;
	using VGrid = VoxelGrid<BGrid, FP_type>;
	using Index_type = typename VGrid::index_type;

	GIVEN("An empty voxel grid") {

		const Index_type axisResolution = 0;
		const FP_type unitLength = 1.0;
		const glm::tvec3<FP_type> aabbMin{ 0.0, 0.0, 0.0 };
		const glm::tvec3<FP_type> aabbMax{ 0.0, 0.0, 0.0 };

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		WHEN("We mipmap with no requested levels") {

			auto mipmapLevels = mipmap(vGrid);

			THEN("No mipmap levels are returned") {
				REQUIRE(mipmapLevels.empty());
			}
		}

		AND_WHEN("We mipmap with 3 requested levels") {

			auto mipmapLevels = mipmap(vGrid, 3);

			THEN("No mipmap levels are returned") {
				REQUIRE(mipmapLevels.empty());
			}
		}
	}

	GIVEN("A voxel grid with 4^3 voxels, with the 8 corner voxels set") {

		const Index_type axisResolution = 4;
		const FP_type unitLength = 1.0;
		const glm::tvec3<FP_type> aabbMin{ 0.0, 0.0, 0.0 };
		const glm::tvec3<FP_type> aabbMax{ 4.0, 4.0, 4.0 };

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		REQUIRE(vGrid.numVoxels() == 64);

		const std::array< glm::tvec3<Index_type>, 8> cornerLocations{ {
			glm::tvec3<Index_type>{0,0,0},
			glm::tvec3<Index_type>{3,0,0},
			glm::tvec3<Index_type>{0,3,0},
			glm::tvec3<Index_type>{3,3,0},
			glm::tvec3<Index_type>{0,0,3},
			glm::tvec3<Index_type>{3,0,3},
			glm::tvec3<Index_type>{0,3,3},
			glm::tvec3<Index_type>{3,3,3}
		} };

		for (const auto& corner : cornerLocations) {
			vGrid[corner] = ElementState::Active;
		}

		WHEN("We mipmap the voxel grid") {

			auto mipmapLevels = mipmap(vGrid);

			THEN("2 mipmap levels are returned. The first level with gridDims {2,2,2} and all voxels active. The second level with gridDims {1,1,1} and its element active") {
				REQUIRE(mipmapLevels.size() == 2);
				REQUIRE(mipmapLevels[0].gridDimensions() == glm::tvec3<Index_type>{2, 2, 2});
				REQUIRE(mipmapLevels[1].gridDimensions() == glm::tvec3<Index_type>{1, 1, 1});


				Index_type activeCount_0 = countActiveVoxels(mipmapLevels[0]);

				REQUIRE(activeCount_0 == 8);

				Index_type activeCount_1 = countActiveVoxels(mipmapLevels[1]);

				REQUIRE(activeCount_1 == 1);

			}
		}

	}

	GIVEN("A voxel grid with 7^3 voxels, with the 8 corner voxels set") {

		const Index_type axisResolution = 7;
		const FP_type unitLength = 1.0;
		const glm::tvec3<FP_type> aabbMin{ 0.0, 0.0, 0.0 };
		const glm::tvec3<FP_type> aabbMax{ 7.0, 7.0, 7.0 };

		const glm::tvec3<Index_type> gridDims{ axisResolution, axisResolution, axisResolution };
		const glm::tvec3<FP_type> unitVoxelDims{ unitLength, unitLength, unitLength };
		const AABB3<FP_type> worldAABB{ aabbMin, aabbMax };

		VGrid vGrid{ gridDims, unitVoxelDims, worldAABB };

		const std::array< glm::tvec3<Index_type>, 8> cornerLocations{ {
			glm::tvec3<Index_type>{0,0,0},
			glm::tvec3<Index_type>{6,0,0},
			glm::tvec3<Index_type>{0,6,0},
			glm::tvec3<Index_type>{6,6,0},
			glm::tvec3<Index_type>{0,0,6},
			glm::tvec3<Index_type>{6,0,6},
			glm::tvec3<Index_type>{0,6,6},
			glm::tvec3<Index_type>{6,6,6}
		} };

		for (const auto& corner : cornerLocations) {
			vGrid[corner] = ElementState::Active;
		}

		WHEN("We mipmap the voxel grid") {

			auto mipmapLevels = mipmap(vGrid);

			THEN("2 mipmap levels are returned. The first level with gridDims {2,2,2} and all voxels active. The second level with gridDims {1,1,1} and its element active") {
				REQUIRE(mipmapLevels.size() == 3);
				REQUIRE(mipmapLevels[0].gridDimensions() == glm::tvec3<Index_type>{4, 4, 4});
				REQUIRE(mipmapLevels[1].gridDimensions() == glm::tvec3<Index_type>{2, 2, 2});
				REQUIRE(mipmapLevels[2].gridDimensions() == glm::tvec3<Index_type>{1, 1, 1});


				Index_type activeCount_0 = countActiveVoxels(mipmapLevels[0]);
				Index_type activeCount_1 = countActiveVoxels(mipmapLevels[1]);
				Index_type activeCount_2 = countActiveVoxels(mipmapLevels[2]);

				REQUIRE(activeCount_0 == 8);
				REQUIRE(activeCount_1 == 8);
				REQUIRE(activeCount_2 == 1);

			}
		}

	}


}