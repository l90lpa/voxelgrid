
#pragma once

#include <glm/vec3.hpp>

template<typename Coord>
struct AABB3
{
	glm::tvec3<Coord> min;
	glm::tvec3<Coord> max;

	bool containes(glm::tvec3<Coord> element) {
		if ((element.x < min.x) && (max.x < element.x)) {
			return false;
		}
		else if ((element.y < min.y) && (max.y < element.y)) {
			return false;
		}
		else if ((element.z < min.z) && (max.z < element.z)) {
			return false;
		}
		else {
			return true;
		}
	}
};