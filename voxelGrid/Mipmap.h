
#pragma once

#include "VoxelGrid.h"

#include <algorithm>
#include <optional>
#include <type_traits>

template<typename Index_T>
Index_T numLODLevels(glm::tvec3<Index_T> gridDims, std::optional<uint32_t> requestedLevels) {
	static_assert(std::is_integral<Index_T>::value);

	// find the largest integer n such that x / 2^n >= 1    <--->   x >= 2^n   <--->   log2(x) >= n   --->   n = floor(log2(x))
	Index_T x_n = std::ceil(std::log2(gridDims.x));
	Index_T y_n = std::ceil(std::log2(gridDims.y));
	Index_T z_n = std::ceil(std::log2(gridDims.z));

	auto maxLevels = std::max({ x_n, y_n, z_n });

	Index_T numLevels = 0;

	if (requestedLevels.has_value()) {
		numLevels = std::clamp(requestedLevels.value(), (Index_T)0, maxLevels);
	}
	else {
		numLevels = maxLevels;
	}

	return numLevels;
}

template<typename Index_T>
glm::tvec3<Index_T> downSampleCoordMap(glm::tvec3<Index_T> coord) {
	return glm::tvec3<Index_T> {
	(coord.x / 2) + (coord.x % 2),
	(coord.y / 2) + (coord.y % 2),
	(coord.z / 2) + (coord.z % 2) };
}

template<typename Block_T, typename Index_T>
BitGrid3<Block_T, Index_T> downSample(const BitGrid3<Block_T, Index_T>& bGrid) {

	using BGrid = BitGrid3<Block_T, Index_T>;
	using Index_type = typename BGrid::index_type;

	// create an empty volume mesh for the current level

	auto sampledGridDims = downSampleCoordMap(bGrid.gridDims);

	BGrid sampledBGrid{ sampledGridDims };

	auto it = bGrid.begin();
	auto end = bGrid.end();
	for (; it != end; ++it) {
		// check if any of the 8 contained voxels are active and if at least one is then set this voxel to active

		bool is_active = *it == true;

		if (is_active) {

			auto position = (*it).position();

			Index_type X = position.x / 2;
			Index_type Y = position.y / 2;
			Index_type Z = position.z / 2;

			sampledBGrid[glm::tvec3<Index_type>{ X, Y, Z }] = true;
		}
	}

	return sampledBGrid;
}

template<typename Block_T, typename Index_T>
std::vector<BitGrid3<Block_T, Index_T>> mipmap(const BitGrid3<Block_T, Index_T>& bGrid, std::optional<uint32_t> requestedLevels = {}) {

	using BGrid = BitGrid3<Block_T, Index_T>;
	using Index_type = typename BGrid::index_type;

	// we should allow selection of the mipmap predicate to be agnostic of the volume mesh data layout

	// Process:
	// - calculate if the number of levels is achieveable and find the max if not
	// - for each level
	//   - for each voxel in the previous level
	//     - if it is active
	//       - compute the coordinate of the current level
	//       - set the associated voxel to active

	auto numLevels = numLODLevels(bGrid.gridDims, requestedLevels);

	std::vector<BGrid> lodBitGrids;

	const BGrid* prevBGrid = &bGrid;
	for (Index_type level = 0; level < numLevels; ++level) {

		auto currBGrid = downSample(*prevBGrid);
		lodBitGrids.emplace_back(std::move(currBGrid));
		prevBGrid = &lodBitGrids.back();

	}

	return lodBitGrids;
}

template<typename BitGrid_T, typename FP_T>
VoxelGrid<BitGrid_T, FP_T> downSample(const VoxelGrid<BitGrid_T, FP_T>& vMesh) {

	using VGrid = VoxelGrid<BitGrid_T, FP_T>;
	using Index_type = typename VGrid::index_type;

	// create an empty volume mesh for the current level

	auto sampledMeshDims = downSampleCoordMap(vMesh.gridDimensions());
	auto scaledUnitVoxelDims = 2.0f * vMesh.unitVoxelDims_;
	VGrid sampledMesh{ sampledMeshDims, scaledUnitVoxelDims, vMesh.world_aabb_ };

	for (auto it = vMesh.begin(), end = vMesh.end(); it != end; ++it) {
		// check if any of the 8 contained voxels are active and if at least one is then set this voxel to active

		bool is_active = *it == ElementState::Active;

		if (is_active) {

			auto position = (*it).gridPosition();

			Index_type X = position.x / 2;
			Index_type Y = position.y / 2;
			Index_type Z = position.z / 2;

			sampledMesh[glm::tvec3<Index_type>{ X, Y, Z }] = ElementState::Active;
		}
	}

	return sampledMesh;
}

template<typename BitGrid_T, typename FP_T>
std::vector<VoxelGrid<BitGrid_T, FP_T>> mipmap(const VoxelGrid<BitGrid_T, FP_T>& vMesh, std::optional<uint32_t> requestedLevels = {}) {

	using VGrid = VoxelGrid<BitGrid_T, FP_T>;
	using Index_type = typename VGrid::index_type;

	// we should allow selection of the mipmap predicate to be agnostic of the volume mesh data layout

	// Process:
	// - calculate if the number of levels is achieveable and find the max if not
	// - for each level
	//   - for each voxel in the previous level
	//     - if it is active
	//       - compute the coordinate of the current level
	//       - set the associated voxel to active

	auto numLevels = numLODLevels(vMesh.gridDimensions(), requestedLevels);

	std::vector<VGrid> lodMeshes;

	const VGrid* prevMesh = &vMesh;
	for (Index_type level = 0; level < numLevels; ++level) {

		auto currMesh = downSample(*prevMesh);
		lodMeshes.emplace_back(std::move(currMesh));
		prevMesh = &lodMeshes.back();

	}

	return lodMeshes;
}