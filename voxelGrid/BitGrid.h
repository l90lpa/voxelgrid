
#pragma once

#include <cassert>
#include <cstdint>
#include <iterator>
#include <vector>
#include <glm/vec3.hpp>
#include <glm/common.hpp>
#include "AABB.h"

// ==================================== Iterator ====================================

// TODOs:
// - replace `&container.grid[0]` where possible to allow the creation of iterators to a container with an empty `grid`
// - move comparison operator outside as a friend function so that we can have the other order lhs == *this
// - decide on consistent using of `this->` and `base::`
// - decide on consistent use of `bit` vs `element`. <--- Prefer `bit`
// - decide on consistent use of `index` vs `location`. <--- Prefer `index`
// - extend interfaces to incorporate more of the expected functionality 
//    - operator->()
//    - operator+(), for advancing iterator
//    - operator-(), for retreating iterator
//    - const_iterator
//    - const_reference
// - change the underlying offset direction within a block???? Maybe.

template<typename BitGrid3_T>
class BaseIterator {
protected:
	using block_type = typename BitGrid3_T::block_type;
	using index_type = std::size_t;
	using difference_type = std::ptrdiff_t;

	BaseIterator( const block_type* ptr, block_type offset, const BitGrid3_T& container) : blockPtr_{ ptr }, offset_{ offset }, container_{ container } { }
	BaseIterator( const BaseIterator& other) : blockPtr_{ other.blockPtr_ }, offset_{ other.offset_ }, container_{ other.container_ } { }
	
	void advance(index_type offset) {
		offset_ += offset;
		blockPtr_ += (offset_ / blockBitCount);
		offset_ %= blockBitCount;
	}

	void retreat(index_type offset) {
		difference_type temp_offset_ = offset_;
		
		temp_offset_ -= static_cast<difference_type>(offset);
		blockPtr -= ((temp_offset_ / blockBitCount) - static_cast<difference_type>(temp_offset_ < 0));
		temp_offset_ %= blockBitCount;
		temp_offset_ += (static_cast<difference_type>(temp_offset_ < 0) * blockBitCount);

		offset_ = temp_offset_;
	}

	block_type mask() const {
		return static_cast<block_type>(1) << this->offset_;
	}

	const block_type* blockPtr_;
	block_type offset_;
	const BitGrid3_T& container_;
	static constexpr block_type blockBitCount{ sizeof(block_type) * 8 };
};

template<typename BitGrid3_T>
class ConstBitReference : public BaseIterator<BitGrid3_T> {
public:
	using base = BaseIterator<BitGrid3_T>;
	using block_type = typename base::block_type;
	using index_type = typename base::index_type;
	using difference_type = typename base::difference_type;
	using value_type = bool;
	using position_type = glm::tvec3<index_type>;

	ConstBitReference(const BaseIterator<BitGrid3_T>& iterator, const block_type* originPtr, position_type gridDims) : base{ iterator }, originPtr_{ originPtr }, gridDims_{ gridDims } {}

	operator value_type() const {
		return static_cast<value_type>((*base::blockPtr_) & base::mask());
	}

	ConstBitReference& operator =(value_type bit) = delete;

	bool operator ==(value_type rhs) const {
		return static_cast<value_type>(*this) == rhs;
	}

	bool operator !=(value_type rhs) const {
		return !(*this == rhs);
	}

	index_type index() const {
		return (blockBitCount * (blockPtr_ - originPtr_)) + offset_;
	}

	position_type position() const {
		index_type i = index();

		index_type x = i % gridDims_.x;
		index_type y = (i / gridDims_.x) % gridDims_.y;
		index_type z = i / (gridDims_.x * gridDims_.y);

		return position_type{ x, y, z };
	}

protected:
	const block_type* originPtr_;
	position_type gridDims_;
};

template<typename BitGrid3_T>
class BitReference : public ConstBitReference<BitGrid3_T> {
public:
	using base = ConstBitReference<BitGrid3_T>;
	using block_type = typename base::block_type;
	using index_type = typename base::index_type;
	using difference_type = typename base::difference_type;
	using value_type = bool;
	using position_type = glm::tvec3<index_type>;

	BitReference( BaseIterator<BitGrid3_T>& iterator, const block_type* originPtr, position_type gridDims) : base{ iterator, originPtr, gridDims } {}

	operator value_type() const {
		return static_cast<value_type>((*base::blockPtr_) & base::mask());
	}

	BitReference& operator =(value_type bit) {
		if (bit) {
			*const_cast<block_type*>(this->blockPtr_) |= mask();
		}
		else {
			*const_cast<block_type*>(this->blockPtr_) &= ~mask();
		}
		return *this;
	}

	bool operator ==(value_type rhs) const {
		return static_cast<value_type>(*this) == rhs;
	}

	bool operator !=(value_type rhs) const {
		return !(*this == rhs);
	}

	// BitReference& flip();
};

template<typename BitGrid3_T>
class ConstIterator : public BaseIterator<BitGrid3_T> {
public:
	using base = BaseIterator<BitGrid3_T>;
	using block_type = typename base::block_type;
	using index_type = typename base::index_type;
	using difference_type = typename base::difference_type;
	using value_type = typename ConstBitReference<BitGrid3_T>::value_type;
	using const_reference = ConstBitReference<BitGrid3_T>;
	//using pointer = ConstBitReference<BitGrid3_T>*;

	ConstIterator(const BitGrid3_T& container) : base{ &container.grid[0], 0, container } { }

	ConstIterator& operator++() { this->advance(1); return *this; }
	ConstIterator& operator++(int junk) { ConstIterator i = *this; this->advance(1); return i; }
	ConstIterator& operator--() { this->retreat(1); return *this; }
	ConstIterator& operator--(int junk) { ConstIterator i = *this; this->retreat(1); return i; }

	ConstIterator operator+(index_type offset) const { ConstIterator i = *this; i.advance(offset); return i; }
	ConstIterator operator-(index_type offset) const { ConstIterator i = *this; i.retreat(offset); return i; }
	ConstIterator& operator+=(index_type offset) { this->advance(offset); return *this; }
	ConstIterator& operator-=(index_type offset) { this->retreat(offset); return *this; }

	const_reference operator*() const {
		return const_reference{ *this, &this->container_.grid[0], this->container_.gridDims };
	}

	bool operator==(const ConstIterator& rhs) const {
		assert(&this->container_ == &rhs.container_);
		return (this->blockPtr_ == rhs.blockPtr_) && (this->offset_ == rhs.offset_);
	}

	bool operator!=(const ConstIterator& rhs) const {
		return !(*this == rhs);
	}
};

template<typename BitGrid3_T>
class Iterator : public ConstIterator<BitGrid3_T> {
public:
	using base = ConstIterator<BitGrid3_T>;
	using block_type = typename base::block_type;
	using index_type = typename base::index_type;
	using difference_type = typename base::difference_type;
	using value_type = typename BitReference<BitGrid3_T>::value_type;
	using reference = BitReference<BitGrid3_T>;
	//using pointer = BitReference<BitGrid3_T>*;

	Iterator(BitGrid3_T& container) : base{ container } { }

	Iterator& operator++() { this->advance(1); return *this; }
	Iterator& operator++(int junk) { Iterator i = *this; this->advance(1); return i; }
	Iterator& operator--() { this->retreat(1); return *this; }
	Iterator& operator--(int junk) { Iterator i = *this; this->retreat(1); return i; }

	Iterator operator+(index_type offset) const { Iterator i = *this; i.advance(offset); return i; }
	Iterator operator-(index_type offset) const { Iterator i = *this; i.retreat(offset); return i; }
	Iterator& operator+=(index_type offset) { this->advance(offset); return *this; }
	Iterator& operator-=(index_type offset) { this->retreat(offset); return *this; }

	reference operator*() {
		return reference{ *this, &this->container_.grid[0], this->container_.gridDims };
	}

	bool operator==(const Iterator& rhs) const {
		assert(&this->container_ == &rhs.container_);
		return (this->blockPtr_ == rhs.blockPtr_) && (this->offset_ == rhs.offset_);
	}

	bool operator!=(const Iterator& rhs) const {
		return !(*this == rhs);
	}
};

// ==================================== Container ====================================

template <typename Block_T = uint32_t, typename Index_T = uint32_t>
class BitGrid3 {
public:
	using block_type = Block_T;
	using index_type = Index_T;

	using position_type = glm::tvec3<index_type>;

	using iterator = Iterator<BitGrid3>;
	using reference = typename iterator::reference;

	using const_iterator = ConstIterator<BitGrid3>;
	using const_reference = typename const_iterator::const_reference;

	BitGrid3() = default;
	BitGrid3(glm::tvec3<index_type> gridDims);

	glm::tvec3<index_type> gridDims;
	std::vector<block_type> grid;

	index_type numElements() const { return gridDims.x * gridDims.y * gridDims.z; }
	index_type numBlocks() const { return grid.size(); }

	BitReference<BitGrid3> operator[](index_type location);
	BitReference<BitGrid3> operator[](glm::tvec3<index_type> position);

	ConstBitReference<BitGrid3> operator[](index_type location) const;
	ConstBitReference<BitGrid3> operator[](glm::tvec3<index_type> position) const;

	block_type* data() { return grid.data(); }
	const block_type* data() const { return grid.data(); }

	glm::tvec3<index_type> locationToPosition(index_type location) const;
	index_type positionToLocation(glm::tvec3<index_type> position) const;

private:
	static constexpr index_type blockMemSize_bits = sizeof(block_type) * 8;
	static constexpr index_type blockMemSize_bytes = sizeof(block_type);
public:
	iterator begin() { return iterator{ *this }; }
	iterator end() { iterator it{ *this }; return it + numElements(); }
	const_iterator begin() const { return const_iterator{ *this }; }
	const_iterator end() const { const_iterator it{ *this }; return it + numElements(); }
	const_iterator cbegin() const { return const_iterator{ *this }; }
	const_iterator cend() const { const_iterator it{ *this }; return it + numElements(); }
};

template<typename Block_T, typename Index_T>
BitGrid3<Block_T, Index_T>::BitGrid3(glm::tvec3<Index_T> gridDims) : gridDims{ gridDims } {

	auto numElements = gridDims.x * gridDims.y * gridDims.z;
	auto numBlocks = (numElements / 32) + ((numElements % 32) > 0);

	grid.resize(numBlocks);
	std::fill(grid.begin(), grid.end(), 0);
}

template<typename Block_T, typename Index_T>
BitReference<BitGrid3<Block_T, Index_T>> BitGrid3<Block_T, Index_T>::operator[](Index_T location) {
	iterator it{ *this };
	it += location;
	return *it;
}

template<typename Block_T, typename Index_T>
BitReference<BitGrid3<Block_T, Index_T>> BitGrid3<Block_T, Index_T>::operator[](glm::tvec3<Index_T> position) {
	return operator[](positionToLocation(position));
}

template<typename Block_T, typename Index_T>
ConstBitReference<BitGrid3<Block_T, Index_T>> BitGrid3<Block_T, Index_T>::operator[](Index_T location) const {
	const_iterator it{ *this };
	it += location;
	return *it;
}

template<typename Block_T, typename Index_T>
ConstBitReference<BitGrid3<Block_T, Index_T>> BitGrid3<Block_T, Index_T>::operator[](glm::tvec3<Index_T> position) const {
	return operator[](positionToLocation(position));
}

template<typename Block_T, typename Index_T>
inline glm::tvec3<Index_T> BitGrid3<Block_T, Index_T>::locationToPosition(Index_T location) const {
	Index_T z = location / (gridDims.x * gridDims.y);
	location -= z * (gridDims.x * gridDims.y);

	Index_T y = location / gridDims.x;
	location -= y * gridDims.x;

	Index_T x = location;

	return glm::tvec3<Index_T>{ x, y, z };
}

template<typename Block_T, typename Index_T>
inline Index_T BitGrid3<Block_T, Index_T>::positionToLocation(glm::tvec3<Index_T> position) const {
	return (position.x + (position.y * gridDims.x) + (position.z * gridDims.x * gridDims.y));
}