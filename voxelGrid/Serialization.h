#pragma once

#include "VoxelGrid.h"

template<typename Block_T, typename Index_T>
void basicASCIISerialization(const BitGrid3<Block_T, Index_T>& bitGrid, std::ostream& outStream) {

	outStream << "Grid Dimensions: {" + std::to_string(bitGrid.gridDims.z) + ", " + 
		std::to_string(bitGrid.gridDims.z) + ", " + std::to_string(bitGrid.gridDims.z) + "}\n\n";

	for (size_t z = 0; z < bitGrid.gridDims.z; ++z) {
		outStream << "z = " << std::to_string(z) << "\n";
		for (size_t y = 0; y < bitGrid.gridDims.y; ++y) {
			for (size_t x = 0; x < bitGrid.gridDims.x; ++x) {

				if (bitGrid[glm::ivec3{ x, y, z }]) {
					outStream << "x ";
				}
				else {
					outStream << ". ";
				}

			}
			outStream << "\n";
		}
		std::cout << "\n\n";
	}
}

template<typename BitGrid_T, typename FP_Coord_T>
void basicASCIISerialization(const VoxelGrid<BitGrid_T, FP_Coord_T>& voxelGrid, std::ostream& outStream) {
	basicASCIISerialization(voxelGrid.bitGrid(), outStream);
}
