
#pragma once

#include "AABB.h"
#include "BitGrid.h"

enum class ElementState {
	Active,
	Inert
};

// ==================================== Iterator ====================================


template<typename VoxelGrid_T>
class VM_ConstReference {
public:
	using voxel_grid = VoxelGrid_T;
	using bitgrid = typename voxel_grid::bitgrid;
	using bitgrid_const_reference = typename bitgrid::const_reference;
	using index_type = typename voxel_grid::index_type;
	using fp_type = typename voxel_grid::fp_type;
	using world_position_type = typename voxel_grid::world_position_type;
	using grid_position_type = typename voxel_grid::grid_position_type;

	VM_ConstReference(bitgrid_const_reference bitRef, const voxel_grid& vMesh) : bitRef_{ bitRef }, vMesh_{ vMesh }, unitVoxelDims_{ vMesh.unitVoxelDims_ }, world_aabb_{ vMesh.world_aabb_ } {}

	operator ElementState() const {
		return to_state(static_cast<bool>(bitRef_));
	}

	VM_ConstReference& operator =(ElementState state) = delete;

	grid_position_type gridPosition() const {
		return bitRef_.position();
	}

	world_position_type worldPosition() const {
		return to_world(bitRef_.position());
	}

private:
	const voxel_grid& vMesh_;
	bitgrid_const_reference bitRef_;
	const world_position_type unitVoxelDims_;
	const AABB3<fp_type> world_aabb_;

	static bool to_bool(const ElementState& state) {
		return (state == ElementState::Active);
	}

	static ElementState to_state(const bool& bit) {
		return (bit) ? ElementState::Active : ElementState::Inert;
	}

	grid_position_type to_grid(const world_position_type& worldPosition) const {

		world_position_type min{ 0.0,0.0,0.0 };
		world_position_type max;
		max.x = (vMesh_.gridDimensions().x > 1) ? vMesh_.gridDimensions().x - 1 : 0;
		max.y = (vMesh_.gridDimensions().y > 1) ? vMesh_.gridDimensions().y - 1 : 0;
		max.z = (vMesh_.gridDimensions().z > 1) ? vMesh_.gridDimensions().z - 1 : 0;

		return static_cast<grid_position_type>(glm::clamp((worldPosition - world_aabb_.min) / unitVoxelDims_, min, max));
	}

	world_position_type to_world(const grid_position_type& gridPosition) const {
		return (static_cast<world_position_type>(gridPosition) * unitVoxelDims_) + world_aabb_.min;
	}
};


template<typename VoxelGrid_T>
class VM_Reference {
public:
	using voxel_grid = VoxelGrid_T;
	using bitgrid = typename voxel_grid::bitgrid;
	using bitgrid_reference = typename bitgrid::reference;
	using index_type = typename voxel_grid::index_type;
	using fp_type = typename voxel_grid::fp_type;
	using world_position_type = typename voxel_grid::world_position_type;
	using grid_position_type = typename voxel_grid::grid_position_type;

	VM_Reference(bitgrid_reference bitRef, const voxel_grid& vMesh) : bitRef_{ bitRef }, vMesh_{ vMesh }, unitVoxelDims_{ vMesh.unitVoxelDims_ }, world_aabb_{ vMesh.world_aabb_ } {}

	operator ElementState() const {
		return to_state(static_cast<bool>(bitRef_));
	}

	VM_Reference& operator =(ElementState state) {
		bitRef_ = to_bool(state);
		return *this;
	};

	grid_position_type gridPosition() const {
		return bitRef_.position();
	}

	world_position_type worldPosition() const {
		return to_world(bitRef_.position());
	}

private:
	const voxel_grid& vMesh_;
	bitgrid_reference bitRef_;
	const world_position_type unitVoxelDims_;
	const AABB3<fp_type> world_aabb_;

	static bool to_bool(const ElementState& state) {
		return (state == ElementState::Active);
	}

	static ElementState to_state(const bool& bit) {
		return (bit) ? ElementState::Active : ElementState::Inert;
	}

	grid_position_type to_grid(const world_position_type& worldPosition) const {

		world_position_type min{ 0.0,0.0,0.0 };
		world_position_type max;
		max.x = (vMesh_.gridDimensions().x > 1) ? vMesh_.gridDimensions().x - 1 : 0;
		max.y = (vMesh_.gridDimensions().y > 1) ? vMesh_.gridDimensions().y - 1 : 0;
		max.z = (vMesh_.gridDimensions().z > 1) ? vMesh_.gridDimensions().z - 1 : 0;

		return static_cast<grid_position_type>(glm::clamp((worldPosition - world_aabb_.min) / unitVoxelDims_, min, max));
	}

	world_position_type to_world(const grid_position_type& gridPosition) const {
		return (static_cast<world_position_type>(gridPosition) * unitVoxelDims_) + world_aabb_.min;
	}
};

template<typename VoxelGrid_T>
class VM_ConstIterator {
public:
	using voxel_grid = VoxelGrid_T;
	using bitgrid = typename voxel_grid::bitgrid;
	using bitgrid_const_iterator = typename bitgrid::const_iterator;
	using index_type = typename voxel_grid::index_type;
	using const_reference = VM_ConstReference<voxel_grid>;

	VM_ConstIterator(bitgrid_const_iterator bitIterator, const voxel_grid& vMesh) : bitIterator_{ bitIterator }, vMesh_{ vMesh } {}

	VM_ConstIterator& operator++() { ++this->bitIterator_; return *this; }
	VM_ConstIterator& operator++(int junk) { VM_ConstIterator i = *this; ++this->bitIterator_; return i; }
	VM_ConstIterator& operator--() { --this->bitIterator_; return *this; }
	VM_ConstIterator& operator--(int junk) { VM_ConstIterator i = *this; --this->bitIterator_; return i; }

	VM_ConstIterator operator+(index_type offset) const { VM_ConstIterator i = *this; i.bitIterator_ += offset; return i; }
	VM_ConstIterator operator-(index_type offset) const { VM_ConstIterator i = *this; i.bitIterator_ -= offset; return i; }
	VM_ConstIterator& operator+=(index_type offset) { this->bitIterator_ += offset; return *this; }
	VM_ConstIterator& operator-=(index_type offset) { this->bitIterator_ -= offset; return *this; }

	const_reference operator*() {
		return const_reference{ *bitIterator_, vMesh_ };
	}

	bool operator==(const VM_ConstIterator& rhs) const {
		assert(&this->vMesh_ == &rhs.vMesh_);
		return this->bitIterator_ == rhs.bitIterator_;
	}
	bool operator!=(const VM_ConstIterator& rhs) const {
		return !(*this == rhs);
	}

private:
	bitgrid_const_iterator bitIterator_;
	const voxel_grid& vMesh_;
};

template<typename VoxelGrid_T>
class VM_Iterator {
public:
	using voxel_grid = VoxelGrid_T;
	using bitgrid = typename voxel_grid::bitgrid;
	using bitgrid_iterator = typename bitgrid::iterator;
	using index_type = typename voxel_grid::index_type;
	using reference = VM_Reference<voxel_grid>;

	VM_Iterator(bitgrid_iterator bitIterator, voxel_grid& vMesh) : bitIterator_{ bitIterator }, vMesh_{ vMesh } {}

	VM_Iterator& operator++() { ++this->bitIterator_; return *this; }
	VM_Iterator& operator++(int junk) { VM_Iterator i = *this; ++this->bitIterator_; return i; }
	VM_Iterator& operator--() { --this->bitIterator_; return *this; }
	VM_Iterator& operator--(int junk) { VM_Iterator i = *this; --this->bitIterator_; return i; }

	VM_Iterator operator+(index_type offset) const { VM_Iterator i = *this; i.bitIterator_ += offset; return i; }
	VM_Iterator operator-(index_type offset) const { VM_Iterator i = *this; i.bitIterator_ -= offset; return i; }
	VM_Iterator& operator+=(index_type offset) { this->bitIterator_ += offset; return *this; }
	VM_Iterator& operator-=(index_type offset) { this->bitIterator_ -= offset; return *this; }

	reference operator*() {
		return reference{ *bitIterator_, vMesh_ };
	}

	bool operator==(const VM_Iterator& rhs) const {
		assert(&this->vMesh_ == &rhs.vMesh_);
		return this->bitIterator_ == rhs.bitIterator_;
	}
	bool operator!=(const VM_Iterator& rhs) const {
		return !(*this == rhs);
	}

private:
	bitgrid_iterator bitIterator_;
	voxel_grid& vMesh_;
};

// ==================================== Container ====================================

template<typename BitGrid_T, typename FP_Coord_T>
class VoxelGrid
{
public:
	using bitgrid = BitGrid_T;
	using index_type = typename BitGrid_T::index_type;
	using grid_position_type = typename bitgrid::position_type;

	using fp_type = FP_Coord_T;
	using world_position_type = glm::tvec3<fp_type>;
	
	using reference = VM_Reference<VoxelGrid>;
	using iterator = VM_Iterator<VoxelGrid>;

	using const_reference = VM_ConstReference<VoxelGrid>;
	using const_iterator = VM_ConstIterator<VoxelGrid>;

	world_position_type unitVoxelDims_;
	AABB3<fp_type> world_aabb_;

	VoxelGrid() = default;
	VoxelGrid(glm::tvec3<index_type> gridDims, glm::tvec3<fp_type> unitVoxelDims, AABB3<fp_type> world_aabb) : bGrid_{ gridDims }, unitVoxelDims_{unitVoxelDims}, world_aabb_{world_aabb} {}

	index_type numVoxels() const {
		return bGrid_.numElements();
	}

	glm::tvec3<index_type> gridDimensions() const {
		return bGrid_.gridDims;
	}

	reference operator[](index_type index) {
		iterator it{ bGrid_.begin(), *this };
		it += index;
		return *it;
	}

	reference operator[](glm::tvec3<index_type> position) {
		return operator[](bGrid_.positionToLocation(position));
	}

	const_reference operator[](index_type index) const {
		const_iterator it{ bGrid_.cbegin(), *this };
		it += index;
		return *it;
	}

	const_reference operator[](glm::tvec3<index_type> position) const {
		return operator[](bGrid_.positionToLocation(position));
	}

	bitgrid& bitGrid() { return bGrid_; }
	const bitgrid& bitGrid() const { return bGrid_; }

	glm::tvec3<index_type> locationToPosition(index_type location) const {
		return bGrid_.locationToPosition(location);
	}

	index_type positionToLocation(glm::tvec3<index_type> position) const {
		return bGrid_.positionToLocation(position);
	}

	iterator begin() { return iterator{ bGrid_.begin(), *this }; }
	iterator end() { iterator it{ bGrid_.begin(), *this }; it += numVoxels(); return it; }

	const_iterator begin() const { return const_iterator{ bGrid_.cbegin(), *this }; }
	const_iterator end() const { const_iterator it{ bGrid_.cbegin(), *this }; it += numVoxels(); return it; }

private:
	BitGrid_T bGrid_;
};


