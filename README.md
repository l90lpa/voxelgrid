Voxel Grid
=====

This is a header-only library for the dense storage and manipulation of voxels. The library has 2 levels of abstraction separating the underlying storage from the user.

- *BitGrid3*: is a class representing a 3 dimensional grid of bits. Underneath the bits are stored contiguously in lexicographic (x,y,z) order.
- *VoxelGrid*: is a class representing a 3 dimensional grid of voxels. Underneath the voxels are represented as bits and thus stored with a BitGrid3 instance. This level of abstraction adds context about the voxels such as there size and world position.

How to build (using CMake)?
-----

Note: given this is a header only library, building will only build the tests. 

The dependencies of this project are:

- [GLM](https://github.com/g-truc/glm)

Dependency management is left to the user but I recommend using VCPKG and simply supplying the VCPKG toolchain file at the command line when running cmake to generate build files. I.e. having cloned and booststrapped VCPKG, then install GLM through VCPKG, we can then do somethinf like `cmake .. -DCMAKE_TOOLCHAIN_FILE="path/to/vcpkg.cmake"` to generate the build files and `cmake . --build` to build the project. 

How to consume (using CMake)?
-----
The CMake build system files provided in this project make available the taget interface library VoxelGrid. This target has appropriate properties set for including the relevant dependencies. Thus a simple why to consume this library into your project is to simply add the CMake command `add_subdirectory(path/to/root/dir/of/VoxelGrid)` to an appropriate place in your build system files and then use either of the CMake commands `target_include_directories(YourTarget PRIVATE VoxelGrid)` or `include_directories(VoxelGrid)`. 